//
//  MyTicketsViewController.swift
//  StudioGenesisDemo
//
//  Created by MasterHardisk on 21/2/21.
//

import UIKit
import Alamofire
import MasterHardiskKit

class MyTicketsViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    let loadingIndicator = LoadingIndicator(text: "Loading...", width: 1.5)
    var tickets: [Ticket] = []
    
    var indexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(self.loadingIndicator)
        self.tableView.tableFooterView = UIView()
        self.searchBar.returnKeyType = .default
        
        hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getTickets()
    }
    
    private func getTickets(){
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        
        AF.request(Constants.url_api + Constants.url_tickets,
                          method: .get,
                          encoding: URLEncoding.default,
                          headers: headers).validate().responseJSON { response in
                            switch response.result {
                            case .success:
                                
                                self.tickets.removeAll()
                                let json = JSON(response.data!)["data"].array!
                                
                                if !json.isEmpty{
                                    for i in 0..<json.count{
                                        self.tickets.append(Ticket(json: json[i]))
                                    }
                                }
                                
                                self.tableView.reloadData()
                            
                                if(!self.loadingIndicator.isHidden)
                                {
                                    self.loadingIndicator.hide()
                                }
                                
                                
                            case .failure:
                                if(!self.loadingIndicator.isHidden)
                                {
                                    self.loadingIndicator.hide()
                                }
                                
                                if let data = response.data{
                                    let jsonError = JSON(data)["errors"]
                                    
                                    AlertControl.showMessage(title: "Error!",
                                                             message: jsonError["message"].stringValue,
                                                            viewController: self)
                                }else {
                                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                          }
        
        
    }
    

    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

}

extension MyTicketsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ticket: Ticket = self.tickets[indexPath.row]
        let cellID = "ticketCellID"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! MyTicketTableViewCell
        
        cell.dateLabel.text = ticket.event_date.toStringWith(format: "EEEE dd MMMM")
        cell.nameLabel.text = ticket.name
        if(!ticket.images.isEmpty){
            ImageUtil.loadImage(imageURL: Constants.url_base + (ticket.images.first?.path)!, completion: { (image) in
                cell.ticketImage.image = image
            })
        }else {
            cell.ticketImage.image = #imageLiteral(resourceName: "placeholder")
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if(segue.identifier == "goTicketDetail"){
            
            let desController = segue.destination as! TicketDetailViewController
            let indexpath = self.tableView.indexPathForSelectedRow
            self.tableView.deselectRow(at: indexpath!, animated: false)
            desController.ticket = self.tickets[indexpath!.row]
        }
        
        if(segue.identifier == "goToEdit"){
            let navController = segue.destination as! UINavigationController
            let desController = navController.topViewController as! AddTicketViewController
            desController.delegate = self
            if let i = self.indexPath{
                desController.ticket = self.tickets[i.row]
            }
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { _, _, complete in
                    self.sendDeleteToServer(indexPath: indexPath)
                    complete(true)
                }
        deleteAction.image = #imageLiteral(resourceName: "ic_trash")
        deleteAction.image?.withTintColor(#colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1))
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        
        return configuration
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction(style: .normal, title: nil) { _, _, complete in
            
            self.indexPath = indexPath
            self.performSegue(withIdentifier: "goToEdit", sender: self)
            
            complete(true)
        }
        
        let image = #imageLiteral(resourceName: "ic_edit")
        image.withTintColor(#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), renderingMode: .alwaysTemplate)
        editAction.image = image
        editAction.backgroundColor = .systemYellow
        
        let configuration = UISwipeActionsConfiguration(actions: [editAction])
        configuration.performsFirstActionWithFullSwipe = true
        
        return configuration
    }
    
    private func sendDeleteToServer(indexPath: IndexPath){
        
        let ticket = self.tickets[indexPath.row]
        
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        
        AF.request(Constants.url_api + Constants.url_tickets.appending("/\(ticket.id!)"),
                          method: .delete,
                          encoding: URLEncoding.default,
                          headers: headers).validate().responseJSON { response in
                            switch response.result {
                            case .success:
                                
                                self.tickets.remove(at: indexPath.row)
                                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                                
                            case .failure:
                                if(!self.loadingIndicator.isHidden)
                                {
                                    self.loadingIndicator.hide()
                                }
                                
                                if let data = response.data{
                                    let jsonError = JSON(data)["errors"]
                                    
                                    AlertControl.showMessage(title: "Error!",
                                                             message: jsonError["message"].stringValue,
                                                            viewController: self)
                                }else {
                                    
                                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                                        self.tableView.reloadData()
                                    }
                                }
                                
                                
                            }
                          }
    }
    
}

extension MyTicketsViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let textToSearch = searchText.replacingOccurrences(of: " ", with: "")
        
        if(textToSearch.count != 0){
            searchTicket(text: textToSearch)
        }else {
            searchBar.resignFirstResponder()
            self.getTickets()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    
    private func searchTicket(text: String)
    {
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        
        AF.request(Constants.url_api + Constants.url_search_tickets + text,
                          method: .get,
                          encoding: URLEncoding.default,
                          headers: headers).validate().responseJSON { response in
                            switch response.result {
                            case .success:
                                
                                self.tickets.removeAll()
                                let json = JSON(response.data!)["data"].array!
                                
                                if !json.isEmpty{
                                    for i in 0..<json.count{
                                        self.tickets.append(Ticket(json: json[i]))
                                    }
                                }
                                
                                self.tableView.reloadData()
                            
                                if(!self.loadingIndicator.isHidden)
                                {
                                    self.loadingIndicator.hide()
                                }
                                
                                
                            case .failure:
                                if(!self.loadingIndicator.isHidden)
                                {
                                    self.loadingIndicator.hide()
                                }
                                
                                if let data = response.data{
                                    let jsonError = JSON(data)["errors"]
                                    
                                    AlertControl.showMessage(title: "Error!",
                                                             message: jsonError["message"].stringValue,
                                                            viewController: self)
                                }else {
                                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                          }
    }
}

extension MyTicketsViewController: TicketDelegate
{
    func ticketUpdate() {
        self.getTickets()
    }
}
