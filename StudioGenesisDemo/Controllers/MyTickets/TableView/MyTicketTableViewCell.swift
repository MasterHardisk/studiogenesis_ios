//
//  MyTicketTableViewCell.swift
//  StudioGenesisDemo
//
//  Created by Gerard Contador on 21/2/21.
//

import UIKit

class MyTicketTableViewCell: UITableViewCell {
    @IBOutlet var ticketImage: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
