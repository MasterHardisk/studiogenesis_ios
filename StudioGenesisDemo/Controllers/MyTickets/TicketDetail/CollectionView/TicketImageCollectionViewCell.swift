//
//  TicketImageCollectionViewCell.swift
//  StudioGenesisDemo
//
//  Created by Gerard Contador on 22/2/21.
//

import UIKit

class TicketImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var image: UIImageView!
    
    
}
