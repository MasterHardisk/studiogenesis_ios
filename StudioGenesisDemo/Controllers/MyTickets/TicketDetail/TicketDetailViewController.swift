//
//  TicketDetailViewController.swift
//  StudioGenesisDemo
//
//  Created by Gerard Contador on 22/2/21.
//

import UIKit
import Alamofire
import MasterHardiskKit

class TicketDetailViewController: UIViewController {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    
    let loadingIndicator = LoadingIndicator(text: "Removing...", width: 1.5)
    var ticket: Ticket!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(self.loadingIndicator)
        self.loadingIndicator.hide()
        
        nameLabel.text = ticket.name
        descriptionLabel.text = ticket.details
        priceLabel.text = "\(ticket.price!)€"
        dateLabel.text = ticket.event_date.toStringWith(format: "EEEE dd MMM yyyy HH:mm'h'")
        if(self.ticket.images.isEmpty || (self.ticket.images.count == 1)){
            self.pageControl.isHidden = true
        }else {
            self.pageControl.numberOfPages = self.ticket.images.count
        }
        
        
    }
    
    @IBAction func deleteTicket(){
        AlertControl.showMessageWithHandler(title: "Delete", message: "Are you sure you want to delete the ticket? This action can not be undone", viewController: self) { (_) in
            
            self.sendDeleteToServer()
        }
    }

    private func sendDeleteToServer(){
        
        self.loadingIndicator.show()
        
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        
        AF.request(Constants.url_api + Constants.url_tickets.appending("/\(ticket.id!)"),
                          method: .delete,
                          encoding: URLEncoding.default,
                          headers: headers).validate().responseJSON { response in
                            switch response.result {
                            case .success:
                                
                                self.loadingIndicator.hide()
                                self.navigationController?.popViewController(animated: true)
                                
                            case .failure:
                                if(!self.loadingIndicator.isHidden)
                                {
                                    self.loadingIndicator.hide()
                                }
                                
                                if let data = response.data{
                                    let jsonError = JSON(data)["errors"]
                                    
                                    AlertControl.showMessage(title: "Error!",
                                                             message: jsonError["message"].stringValue,
                                                            viewController: self)
                                }else {
                                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                }
                            }
                          }
    }
}

extension TicketDetailViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.ticket.images.isEmpty)
        {
            return 1
        }else {
            return self.ticket.images.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellID = "image_ticketID"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! TicketImageCollectionViewCell
        
        if(!ticket.images.isEmpty){
            let imageTicket = ticket.images[indexPath.row]
            
            ImageUtil.loadImage(imageURL: Constants.url_base + imageTicket.path) { (image) in
                cell.image.image = image
            }
        }
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width, height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print(indexPath.row)
        self.pageControl.currentPage = indexPath.row
    }
}
