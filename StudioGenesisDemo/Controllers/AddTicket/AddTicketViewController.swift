//
//  AddTicketViewController.swift
//  StudioGenesisDemo
//
//  Created by Gerard Contador on 23/2/21.
//

import UIKit
import Alamofire
import MasterHardiskKit

class AddTicketViewController: UIViewController {

    @IBOutlet var addPhoto1: UIButton!
    @IBOutlet var addPhoto2: UIButton!
    @IBOutlet var addPhoto3: UIButton!
    @IBOutlet var addPhoto4: UIButton!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var descriptionField: UITextField!
    @IBOutlet var price: UITextField!
    @IBOutlet var dataTicket: UIDatePicker!
    @IBOutlet var saveButton: UIButton!
    
    var delegate: TicketDelegate?

    let savingIndicator = LoadingIndicator(text: "Saving...", width: 1.5)
    var currentButton: UIButton!
    var images: [UIImage] = []
    var imageDelegate: ImageUtilDelegate?
    var ticket: Ticket?
    
    private let imagePicker = UIImagePickerController()
    private var setImage1 = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        imageDelegate = self
        
        self.view.addSubview(self.savingIndicator)
        self.savingIndicator.hide()
        
        hideKeyboardWhenTappedAround()
        
        if let t = ticket{
            nameField.text = t.name
            descriptionField.text = t.details
            price.text = "\(t.price!)"
            dataTicket.date = t.event_date
            
            saveButton.setTitle("Update", for: .normal)
            
            if !t.images.isEmpty {
                for i in 0..<t.images.count {
                    if let tmpButton = self.view.viewWithTag(i+20) as? UIButton {
                        print(tmpButton.tag)
                        
                        ImageUtil.loadImage(imageURL: Constants.url_base + t.images[i].path) { (image) in
                            self.images.append(image)
                            tmpButton.setImage(image, for: .normal)
                            self.setImage1=true
                        }
                    }
                }
            }
        }
    }

    @IBAction func addPhoto(_ sender: UIButton) {
        currentButton = sender
        
        if (currentButton.currentImage!.description.contains("ic_camera")) {
            ImageUtil.getImageFrom(viewController: self, imagePicker: imagePicker, title: "Add image", withDelete: false)
         }
         else {
            ImageUtil.getImageFrom(viewController: self, imagePicker: imagePicker, title: "Add image", withDelete: true)
         }
        
        
    }
    
    
    @IBAction func saveTicket(){
        if(!self.setImage1){
            AlertControl.showMessage(title: "Error!", message: "Add at least one image", viewController: self)
        }
        else if(!self.checkField()){
            AlertControl.showMessage(title: "Error!", message: "Fill in all the fields", viewController: self)
        }else {

            sendTicketToServer()
        }
        
    }
    
    
    private func sendTicketToServer(){
        self.savingIndicator.show()
        
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        var url = ""
        if let t = self.ticket {
            url = Constants.url_api + Constants.url_tickets.appending("/\(t.id!)")
        }else {
            url = Constants.url_api + Constants.url_tickets
        }
        
        let parametersList = ["name", "description", "price", "event_date"]
        
        AF.upload(multipartFormData: { multipartFormData in
            
            for i in 0..<self.images.count {
                print(i)
                let imageData = self.images[i].jpegData(compressionQuality: 0.8)
                let imageName = String.random(length: 8)
                multipartFormData.append(imageData!, withName: "image\(i)", fileName: "\(imageName).jpg", mimeType: "image/png")
            }
            
            
            multipartFormData.append((self.nameField.text?.data(using: .utf8))!, withName: parametersList[0])
            multipartFormData.append((self.descriptionField.text?.data(using: .utf8))!, withName: parametersList[1])
            multipartFormData.append((self.price.text?.data(using: .utf8))!, withName: parametersList[2])
            multipartFormData.append(self.dataTicket.date.toStringWith(format: "dd-MM-yyyy HH:mm").data(using: .utf8)!, withName: parametersList[3])
            
        }, to: url, method: .post, headers: headers).responseJSON(completionHandler: { (response) in
            if(response.response?.statusCode == 200){
                self.savingIndicator.hide()
                
                let alert = UIAlertController(title: "Success", message: "The ticket was saved successfully", preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)

               
                let when = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: when){
                    alert.dismiss(animated: true) {
                        self.dismiss(animated: true) {
                            self.delegate?.ticketUpdate()
                        }
                    }
                }
            }else {
                if(!self.savingIndicator.isHidden)
                {
                    self.savingIndicator.hide()
                }
                if let data = response.data{
                    let jsonError = JSON(data)["errors"]
                    var error: String = ""
                    for parameter in parametersList {
                        if let e = jsonError[parameter].arrayObject {
                            error.append(e.compactMap({($0 as! String)}).joined())
                        }
                    }
                    if(error.isEmpty){
                        AlertControl.showMessage(title: "Error!",
                                                 message: jsonError["message"].stringValue,
                                                viewController: self)
                    }else {
                        AlertControl.showMessage(title: "Error!",
                                                 message: error,
                                                viewController: self)
                    }
                }else {
                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        })
    }
    
    private func checkField()-> Bool{
        if(self.nameField.text!.isEmpty || self.descriptionField.text!.isEmpty || self.price.text!.isEmpty)
        {
            return false
        }
        
        return true
    }
}

extension AddTicketViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("image picker")
        
        guard let myImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
               else {
                   print("Image not found...")
                   return
               }
        
        
        self.imagePicker.dismiss(animated: true, completion: nil)
        
        self.imageDelegate?.imageSelect(image: myImage)
        
    }
}

extension AddTicketViewController: ImageUtilDelegate{
    func imageDelete() {
        self.currentButton.setImage(#imageLiteral(resourceName: "ic_camera"), for: .normal)
        self.images.remove(at: self.currentButton.tag-20)
        if self.images.isEmpty {
            self.setImage1 = false
        }
    }
    
    func imageSelect(image: UIImage) {
        if self.setImage1 && !currentButton.currentImage!.description.contains("ic_camera"){
            self.images.remove(at: self.currentButton.tag-20)
        }
        self.currentButton.setImage(image, for: .normal)
        self.currentButton.setTitle("", for: .normal)
        self.images.insert(image, at: self.currentButton.tag-20)
        self.setImage1 = true
    }
    
    
}

protocol TicketDelegate: class {
    func ticketUpdate()
}

