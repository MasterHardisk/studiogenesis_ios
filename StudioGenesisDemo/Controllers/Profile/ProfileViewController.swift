//
//  ProfileViewController.swift
//  StudioGenesisDemo
//
//  Created by Gerard Contador on 19/2/21.
//

import UIKit
import MasterHardiskKit
import Alamofire

class ProfileViewController: UIViewController {

    @IBOutlet var image_profile: UIButton!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var surnameField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var birthdayField: UIDatePicker!
    
    private let imagePicker = UIImagePickerController()
    
    let loadingIndicator = LoadingIndicator(text: "Loading...", width: 1.5)
    let savingIndicator = LoadingIndicator(text: "Saving...", width: 1.5)
    var currentDate: Date?
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.loadingIndicator)
        self.view.addSubview(self.savingIndicator)
        self.savingIndicator.hide()
        
        self.nameField.delegate = self
        self.emailField.delegate = self
        self.surnameField.delegate = self
        
        self.imagePicker.delegate = self
        
        birthdayField.addTarget(self, action: #selector(datePickerEditingDidEnd), for: UIControl.Event.editingDidEnd)
        
        birthdayField.addTarget(self, action: #selector(datePickerEditingDidBegin), for: UIControl.Event.editingDidBegin)
        
        hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadUserData()
    }
    

    
    @objc private func datePickerEditingDidBegin(){
        currentDate = self.birthdayField.date
        
    }
    
    @objc private func datePickerEditingDidEnd(){
        if(birthdayField.date != currentDate!){
            updateUser(data: birthdayField.date.toStringWith(format: "dd-MM-yyyy"), type: "b_day")
        }
        
    }
    
    private func loadUserData(){
       
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        
        AF.request(Constants.url_api + Constants.url_users,
                          method: .get,
                          encoding: URLEncoding.default,
                          headers: headers).validate().responseJSON { response in
                            switch response.result {
                            case .success:
                                
                                let json = JSON(response.data!)["data"]
                            
                                self.user = User(json: json)
                                
                                self.nameField.text = self.user!.name
                                self.surnameField.text = self.user!.surname
                                
                                if let birthdate = self.user?.b_day {
                                    self.birthdayField.setDate(birthdate, animated: false)
                                }
                                
                                self.emailField.text = self.user!.email
                                
                                if let url = self.user!.profile {
                                    ImageUtil.loadImage(imageURL: Constants.url_base + url) { (image) in
                                        self.image_profile.setImage(image, for: .normal)
                                    }
                                }
                                
                                
                                if(!self.loadingIndicator.isHidden)
                                {
                                    self.loadingIndicator.hide()
                                }
                                
                                
                            case .failure:
                                if(!self.loadingIndicator.isHidden)
                                {
                                    self.loadingIndicator.hide()
                                }
                                
                                if let data = response.data{
                                    let jsonError = JSON(data)["errors"]
                                    
                                    AlertControl.showMessage(title: "Error!",
                                                             message: jsonError["message"].stringValue,
                                                            viewController: self)
                                }else {
                                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                }
                            }
                          }
    }
    
    
    @IBAction func logout(_ sender: Any) {
        
        AlertControl.showMessageWithHandler(title: "Sign off", message: "Are you sure you want to log out?", viewController: self) { (_) in
            
            self.performSegue(withIdentifier: "goToLogin", sender: nil)
            
        }
    }
    
    @IBAction func addPhoto(_ sender: UIButton) {
        self.imagePicker.allowsEditing = true
        ImageUtil.getImageFrom(viewController: self, imagePicker: imagePicker, title: "Change profile picture", withDelete: false)
    }
    
}

extension ProfileViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField.tag {
        case 0:
            self.updateUser(data: textField.text!, type: "name")
        case 1:
            self.updateUser(data: textField.text!, type: "surname")
        case 2:
            if(!(self.user?.email.elementsEqual(textField.text!))!)
            {self.updateUser(data: textField.text!, type: "email")}
            
        default:
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    private func updateUser(data: String, type: String){
        self.savingIndicator.show()
        
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        
        let parameters : Parameters = [
            type: data,
        ]
        
        AF.request(Constants.url_api + Constants.url_users.appending("/update/\(type)"),
                          method: .put,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: headers).validate().responseJSON { response in
                            switch response.result {
                            case .success:
                                
                                let json = JSON(response.data!)["data"]
                            
                                self.user = User(json: json)
                                
                                self.nameField.text = self.user!.name
                                self.surnameField.text = self.user!.surname
                                if let birthdate = self.user?.b_day {
                                    self.birthdayField.setDate(birthdate, animated: false)
                                }
                                
                                self.emailField.text = self.user!.email
                                
                                
                                if let url = self.user!.profile {
                                    ImageUtil.loadImage(imageURL: Constants.url_base + url) { (image) in
                                        self.image_profile.setImage(image, for: .normal)
                                    }
                                }
                                
                                if(!self.savingIndicator.isHidden)
                                {
                                    self.savingIndicator.hide()
                                }
                                
                                
                            case .failure:
                                if(!self.savingIndicator.isHidden)
                                {
                                    self.savingIndicator.hide()
                                }
                                
                                if let data = response.data{
                                    let jsonError = JSON(data)["errors"]
                                    
                                    AlertControl.showMessage(title: "Error!",
                                                             message: jsonError["message"].stringValue,
                                                            viewController: self)
                                }else {
                                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                }
                            }
                          }
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("image picker")
        
        guard let myImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
               else {
                   print("Image not found...")
                   return
               }
        self.imagePicker.dismiss(animated: true) {
            self.changeImageProfile(image: myImage)
        }
    }
    
    private func changeImageProfile(image: UIImage){
        self.savingIndicator.show()
        
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        
        AF.upload(multipartFormData: { multipartFormData in
            
            
            let imageData = image.jpegData(compressionQuality: 0.8)
            let imageName = String.random(length: 8)
            multipartFormData.append(imageData!, withName: "profile", fileName: "\(imageName).jpg", mimeType: "image/png")
            
            
        }, to: Constants.url_api + Constants.url_users.appending("/update/image_profile"), method: .post, headers: headers).responseJSON(completionHandler: { (response) in
        
            if(response.response?.statusCode == 200){
                self.savingIndicator.hide()
                
                let json = JSON(response.data!)["data"]
            
                self.user = User(json: json)
                
                self.nameField.text = self.user!.name
                self.surnameField.text = self.user!.surname
                if let birthdate = self.user?.b_day {
                    self.birthdayField.setDate(birthdate, animated: false)
                }
                
                self.emailField.text = self.user!.email
                
                
                if let url = self.user!.profile {
                    ImageUtil.loadImage(imageURL: Constants.url_base + url) { (image) in
                        self.image_profile.setImage(image, for: .normal)
                    }
                }
                
            }else {
                if(!self.savingIndicator.isHidden)
                {
                    self.savingIndicator.hide()
                }
                
                print(response)
                if let data = response.data{
                    let jsonError = JSON(data)["errors"]
                    print(jsonError)
                    
                    AlertControl.showMessage(title: "Error!",
                                             message: jsonError["message"].stringValue,
                                            viewController: self)
                   
                }else {
                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        })
       
    }
}
