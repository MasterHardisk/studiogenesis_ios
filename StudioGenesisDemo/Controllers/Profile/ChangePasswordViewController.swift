//
//  ChangePasswordViewController.swift
//  StudioGenesisDemo
//
//  Created by Gerard Contador on 23/2/21.
//

import UIKit
import Alamofire
import MasterHardiskKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet var currentPassField: UITextField!
    @IBOutlet var newPassField: UITextField!
    @IBOutlet var confirmationPassField: UITextField!
    
    let savingIndicator = LoadingIndicator(text: "Saving...", width: 1.5)
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(self.savingIndicator)
        self.savingIndicator.hide()
        
        self.currentPassField.delegate = self
        self.newPassField.delegate = self
        self.confirmationPassField.delegate = self
        
        hideKeyboardWhenTappedAround()
    }
    

    @IBAction func changePasswordAction(){
        if(!checkField())
        {
            AlertControl.showMessage(title: "Atention", message: "Fill in all the fields", viewController: self)
        }else
        {
            self.sendNewPassToServer()
        }
    }
    
    private func sendNewPassToServer(){
        self.savingIndicator.show()
        
        let headers : HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": UserInfo.get(type: String.self, forKey: .token)
        ]
        
        let parametersList = ["current_password", "password", "password_confirmation"]
        
        let parameters : Parameters = [
            parametersList[0]: currentPassField.text!,
            parametersList[1]: newPassField.text!,
            parametersList[2]: confirmationPassField.text!,
        ]
        
        
        AF.request(Constants.url_api + Constants.url_update_password,
                          method: .put,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: headers).validate().responseJSON { response in
                            switch response.result {
                            case .success:
                                
                                self.savingIndicator.hide()
                                let json = JSON(response.data!)["data"]
                                
                                let alert = UIAlertController(title: "Success", message: json["result"].stringValue, preferredStyle: .alert)
                                self.present(alert, animated: true, completion: nil)

                               
                                let when = DispatchTime.now() + 2
                                DispatchQueue.main.asyncAfter(deadline: when){
                                    alert.dismiss(animated: true) {
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                }
                                
                                
                            case .failure:
                                if(!self.savingIndicator.isHidden)
                                {
                                    self.savingIndicator.hide()
                                }
                                
                                print(response)
                                if let data = response.data{
                                    let jsonError = JSON(data)["errors"]
                                    print(jsonError)
                                    
                                    var error: String = ""
                                    for parameter in parametersList {
                                        if let e = jsonError[parameter].arrayObject {
                                            error.append(e.compactMap({($0 as! String)}).joined())
                                        }
                                    }
                                    
                                    if(error.isEmpty){
                                        AlertControl.showMessage(title: "Error!",
                                                                 message: jsonError["message"].stringValue,
                                                                viewController: self)
                                    }else {
                                        AlertControl.showMessage(title: "Error!",
                                                                 message: error,
                                                                viewController: self)
                                    }
                                   
                                
                                    
                                    
                                       
                                
                                }else {
                                    AlertControl.messageErrorWithHandler(viewController: self) { (_) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                }
                            }
                          }
        
    }
    
    private func checkField()-> Bool
    {
        if(self.currentPassField.text!.isEmpty || self.newPassField.text!.isEmpty || self.confirmationPassField.text!.isEmpty)
        {
            return false
        }
        return true
    }

}

extension ChangePasswordViewController: UITextFieldDelegate{

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.currentPassField {
            self.newPassField.becomeFirstResponder()
        }
        if textField == self.newPassField{
            self.confirmationPassField.becomeFirstResponder()
        }
        if textField == self.confirmationPassField{
            self.confirmationPassField.resignFirstResponder()
        }
        
        return true
    }
}

