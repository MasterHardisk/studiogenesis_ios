//
//  ViewController.swift
//  StudioGenesisDemo
//
//  Created by MasterHardisk on 18/2/21.
//

import UIKit
import MasterHardiskKit
import Alamofire

class LoginViewController: UIViewController {

    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    let loadingIndicator = LoadingIndicator(text: "logging in...", width: 1.5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    
        self.view.addSubview(self.loadingIndicator)
        self.loadingIndicator.hide()
        
        self.emailField.delegate = self
        self.passwordField.delegate = self
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(UserInfo.exits(key: .email_field))
        {
            emailField.text = UserInfo.get(type: String.self, forKey: .email_field)
        }
        
        self.goMain()
    }
    
    private func goMain(){
        if(UserInfo.get(type: Bool.self, forKey: .loged)){
            performSegue(withIdentifier: "goToMain", sender: nil)
        }
    }

    @IBAction func login(){
        if(checkField())
        {
            if(validateEmail(enteredEmail: emailField.text!)){
                
                self.loadingIndicator.show()
                
                let parameters : Parameters = [
                    "email": emailField.text!,
                    "password": passwordField.text!,
                ]
                
                let headers : HTTPHeaders = [
                    "Accept": "application/json",
                ]
                
                AF.request(Constants.url_api + Constants.url_login,
                                  method: .post,
                                  parameters: parameters,
                                  encoding: URLEncoding.default,
                                  headers: headers).validate().responseJSON { response in
                                    switch response.result {
                                    case .success:
                                        
                                        let userJson = JSON(response.data!)["data"]
                                    
                                        UserInfo.set(value: userJson["token"].string, key: .token)
                                        UserInfo.set(value: self.emailField.text, key: .email_field)
                                        UserInfo.set(value: true, key: .loged)
                                        
                                        self.loadingIndicator.hide()
                                        self.performSegue(withIdentifier: "goToMain", sender: nil)
                                        self.passwordField.text = ""
                                        
                                    case .failure:
                                        self.loadingIndicator.hide()
                                        
                                        let jsonError = JSON(response.data!)["errors"]
                                       
                                        AlertControl.showMessage(title: "Error!",
                                                                 message: jsonError["message"].stringValue,
                                                                viewController: self)
                                        
                                    }
                                  }
            }else
            {
                AlertControl.showMessage(title: "Attention!",
                                         message: "The format of the email entered is incorrect",
                                        viewController: self)
            }
        }else
        {
            AlertControl.showMessage(title: "Attention!",
                                     message: "Fill in all the fields",
                                    viewController: self)
        }
       
        
    }
    
    
    private func checkField() -> Bool{
        return (emailField.hasText && passwordField.hasText)
    }
    
    private func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    @IBAction func unwindInit(segue:UIStoryboardSegue) {
         UserInfo.removeAllData()
    }

}

extension LoginViewController: UITextFieldDelegate{

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField ==  self.emailField{
            self.passwordField.becomeFirstResponder()
        }
        if textField == self.passwordField{
            self.passwordField.resignFirstResponder()
        }
        return true
    }
}

