//
//  ImageUtil.swift
//  StudioGenesisDemo
//
//  Created by MasterHardisk on 19/2/21.
//

import UIKit
import PDFKit
import MasterHardiskKit
import Alamofire
import Photos

public class ImageUtil {
    
    private static var imageCache: NSCache = NSCache<NSString, UIImage>()
    private static var delegate: ImageUtilDelegate?
    
    public class func loadImage(imageURL: String, completion: @escaping (_ image: UIImage)->()){
       
        if let imageFileURL = self.imageCache.object(forKey: imageURL as NSString){
            
            completion(imageFileURL)
        
        }else if let url = URL.init(string: imageURL) {
           
        
            URLSession.shared.dataTask(with: url) { (data, response, error) in
            
                guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    
                    let data = data, error == nil,
                    
                    let image = UIImage(data: data)
                    
                    else { return }
                
                DispatchQueue.main.async() { () -> Void in
                
                    self.imageCache.setObject(image, forKey: imageURL as NSString)
                    
                    completion(image)
                }
                }.resume()
        }
    }
    
    public class func getImageFrom(viewController: UIViewController, imagePicker: UIImagePickerController, title: String, withDelete: Bool) {
        
        
        let alertController = UIAlertController(title: title,
                                                message: "Select an option",
                                                preferredStyle: .actionSheet)
        
        
        let fromLibrary = UIAlertAction(title: "From library",
                                        style: .default) { _ in
                                            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                                                checkPermission()
                                                imagePicker.allowsEditing = true
                                                imagePicker.sourceType = .photoLibrary
                                                viewController.present(imagePicker, animated: true, completion: nil)
                                            }
        }
        
        let fromCamera = UIAlertAction(title: "From camera",
                                       style: .default) { _ in
                                        if UIImagePickerController.isSourceTypeAvailable(.camera) {
                                            imagePicker.sourceType = .camera
                                            imagePicker.allowsEditing = true
                                            imagePicker.showsCameraControls = true
                                            viewController.present(imagePicker, animated: true, completion: nil)
                                        }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        
        alertController.addAction(fromLibrary)
        alertController.addAction(fromCamera)
        if(withDelete){
            let delete = UIAlertAction(title: "Delete", style: .default) { (_) in
                delegate = (viewController as! ImageUtilDelegate)
                self.delegate?.imageDelete()
            }
            
            alertController.addAction(delete)
        }
       
        
        
        alertController.addAction(cancel)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    class func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            print("User do not have access to photo album.")
        case .denied:
            print("User has denied the permission.")
        case .limited:
            print("User has limited the permission.")
        @unknown default:
             print("User has denied the permission.")
        }
    }
}

protocol ImageUtilDelegate: class {
    func imageSelect(image: UIImage)
    func imageDelete()
}
