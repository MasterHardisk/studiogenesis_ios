//
//  LoadingIndicator.swift
//  StudioGenesisDemo
//
//  Created by MasterHardisk on 18/2/21.
//

import UIKit

class LoadingIndicator: UIVisualEffectView {
    
    var text: String? {
        didSet {
            label.text = text
        }
    }
    let color = #colorLiteral(red: 0.368627451, green: 0.3607843137, blue: 0.9019607843, alpha: 1)
    
    var width: CGFloat?
    
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
    let label: UILabel = UILabel()
    let blurEffect = UIBlurEffect(style: .light)
    let vibrancyView: UIVisualEffectView
    
    init(text: String, width: CGFloat) {
        self.text = text
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(effect: blurEffect)
        self.setup()
        self.width = width
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.text = ""
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
        contentView.addSubview(vibrancyView)
        contentView.addSubview(activityIndictor)
        contentView.addSubview(label)
        activityIndictor.startAnimating()
    }
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let superview = self.superview {
            
            let width = superview.frame.size.width / self.width!
            let height: CGFloat = 50.0
            self.frame = CGRect(x: superview.frame.size.width / 2 - width / 2,
                                y: superview.frame.height / 2 - height / 2,
                                width: width,
                                height: height)
            vibrancyView.frame = self.bounds
            
            let activityIndicatorSize: CGFloat = 40
            activityIndictor.frame = CGRect(x: 5,
                                            y: height / 2 - activityIndicatorSize / 2,
                                            width: activityIndicatorSize,
                                            height: activityIndicatorSize)
            
            activityIndictor.color = color
            
            
            layer.cornerRadius = 8.0
            layer.masksToBounds = true
            label.text = text
            label.textAlignment = NSTextAlignment.center
            label.frame = CGRect(x: activityIndicatorSize + 5,
                                 y: 0,
                                 width: width - activityIndicatorSize - 15,
                                 height: height)
            label.textColor = color
            label.font = UIFont.boldSystemFont(ofSize: 13)
            
            if #available(iOS 12.0, *) {
                if (UIScreen.main.traitCollection.userInterfaceStyle == .dark){
                    label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    activityIndictor.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                else {
                    label.textColor = color
                    activityIndictor.color = color
                }
            }
            
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if #available(iOS 12.0, *) {
            let userInterfaceStyle = traitCollection.userInterfaceStyle
             
            switch userInterfaceStyle {
            case .light:
                label.textColor = color
                activityIndictor.color = color
                break
            case .dark:
                label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                activityIndictor.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                break
            case .unspecified:
                label.textColor = color
                activityIndictor.color = color
            @unknown default:
                label.textColor = color
                activityIndictor.color = color
            }
            self.reloadInputViews()
        }
       
    }
    
    func show() {
        self.isHidden = false
        superview?.isUserInteractionEnabled = false
    }
    
    func hide() {
        self.isHidden = true
        superview?.isUserInteractionEnabled = true
    }
}

