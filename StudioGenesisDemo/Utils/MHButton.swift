//
//  MHButton.swift
//  StudioGenesisDemo
//
//  Created by MasterHardisk on 18/2/21.
//

import UIKit

@IBDesignable open class MHButton: UIButton{
    private var image = UIImageView()
    private var label = UILabel()
    private var notificationLabel = UILabel()
    
    @IBInspectable
    public var showBadge: Bool = false {
        didSet {
            if showBadge {
                self.notificationLabel.backgroundColor = #colorLiteral(red: 1, green: 0.2705882353, blue: 0.2274509804, alpha: 1)
                self.notificationLabel.text = "!"
            }else {
                self.notificationLabel.textColor = UIColor.clear
                self.notificationLabel.backgroundColor = UIColor.clear
                
            }
        }
    }
    
    @IBInspectable
    var icon: UIImage?{
        didSet {
            image = UIImageView(frame: CGRect(x: self.frame.size.width / 3.5, y: self.frame.height/2 - image.frame.width/2 - 25 , width: sizeIcon.width, height: sizeIcon.height))
            image.contentMode = .scaleAspectFit
            image.image = icon
            self.addSubview(image)
            
            setupView()
        }
    }
    
   
    
    @IBInspectable
    var color: UIColor = UIColor.lightGray {
           didSet {
            image.tintColor = color
            label.textColor = color
           }
    }
    
    @IBInspectable
    var sizeIcon: CGSize = CGSize(width: 40, height: 40) {
        didSet{
            image.frame = CGRect(x: icon_position.x, y: icon_position.y, width: sizeIcon.height, height: sizeIcon.width)
            
        }
    }
    
    @IBInspectable
    var icon_position: CGPoint = CGPoint(x: 0, y: 0){
        didSet{
            self.image.frame = CGRect(x: icon_position.x, y: icon_position.y, width: image.frame.width, height: image.frame.height)
        }
    }
        
    @IBInspectable
    var text: String?{
        didSet{
            label = UILabel(frame: CGRect(x: text_position.x, y: text_position.y, width: self.frame.width, height: 18))
            
            label.text = NSLocalizedString(text!, comment: text!)
            label.font = fontText
            label.textAlignment = .center
            label.textColor = color
            self.addSubview(label)
        }
    }
    
    @IBInspectable
    var text_position: CGPoint = CGPoint(x: 0, y: 60){
        didSet{
            self.label.frame = CGRect(x: text_position.x, y: text_position.y, width: self.frame.width, height: 18)
        }
    }
    
    @IBInspectable
    var text_size: CGFloat = 12{
        didSet{
            fontText = UIFont(name: "Avenir Next", size: text_size)!
            label.font = fontText
            label.sizeToFit()
            label.frame.origin.x = self.frame.size.width / 2 - label.frame.width / 2
            
        }
    }

    var fontText: UIFont = UIFont(name: "Avenir Next", size: 12)!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required public init?(coder: NSCoder) {
           super.init(coder: coder)
        
        addTarget(self, action: #selector(touchDown(_:)), for: .touchDown)
        addTarget(self, action: #selector(touchUp(_:)), for: .touchUpInside)
        setupView()
    }
    
    func setupView() {
        self.frame.size.height = self.frame.width
        self.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1.0/1.0).isActive = true
        self.layer.cornerRadius = self.frame.height / 2

        self.setTitle("", for: .normal)
    
        self.layer.shadowColor = #colorLiteral(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 1.0
        
        self.notificationLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        
        self.notificationLabel.alpha = 0.9
        self.notificationLabel.layer.cornerRadius = 10
        self.notificationLabel.clipsToBounds = true
        self.notificationLabel.isUserInteractionEnabled = false
        self.notificationLabel.translatesAutoresizingMaskIntoConstraints = false
        self.notificationLabel.textAlignment = .center
        self.notificationLabel.textColor = UIColor.white
        self.notificationLabel.layer.zPosition = -1
        
        self.addSubview(self.notificationLabel)
        
        self.notificationLabel.widthAnchor.constraint(equalToConstant: 20).isActive = true
        self.notificationLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.notificationLabel.centerXAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        self.notificationLabel.centerYAnchor.constraint(equalTo: self.topAnchor, constant: 15).isActive = true
        
    }
    
    @objc func touchDown(_ sender: UIButton){
        self.layer.shadowColor = UIColor.clear.cgColor
        let impactFeedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        impactFeedbackGenerator.impactOccurred()

    }
    
    
    @objc func touchUp(_ sender: UIButton){
        self.layer.shadowColor = #colorLiteral(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        let impactFeedbackGenerator = UIImpactFeedbackGenerator(style: .medium)
        impactFeedbackGenerator.impactOccurred()
    }
}
