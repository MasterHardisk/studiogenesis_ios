//
//  UserInfo.swift
//  StudioGenesisDemo
//
//  Created by MasterHardisk on 18/2/21.
//

import UIKit
import MasterHardiskKit

final class UserInfo
{
    enum Key: String, CaseIterable {
        case token, loged, email_field
    }
    
    static func set<T>(value: T, key: Key){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key.rawValue)
    }
    
    static func get<T>(type: T.Type, forKey: Key) -> T {
        let defaults = UserDefaults.standard
        let value = defaults.object(forKey: forKey.rawValue) as? T
        if T.self == Bool.self && value == nil {
            return false as! T
        }else {return value!}
    }
    
    static func exits(key: Key) -> Bool {
        let defaults = UserDefaults.standard
        return defaults.object(forKey: key.rawValue) != nil
    }
    
    static func removeValueFor(key: Key) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key.rawValue)
    }
    
    static func removeAllData() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: Key.token.rawValue)
        defaults.removeObject(forKey: Key.loged.rawValue)
    }
}

