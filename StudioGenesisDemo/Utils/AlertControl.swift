//
//  AlertControl.swift
//  StudioGenesisDemo
//
//  Created by Gerard Contador on 18/2/21.
//

import UIKit

open class AlertControl
{
    class func showMessage(title: String, message:String, viewController: UIViewController){
        
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Accept", style: .destructive, handler: nil)

        alertController.addAction(okAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    class func messageError(viewController: UIViewController){
        let alertController = UIAlertController(title: "Error!",
                                                message: "An unknown error has occurred, please try again later",
                                                preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Accept", style: .destructive, handler: nil)

        alertController.addAction(okAction)
        
        viewController.present(alertController, animated: true, completion: nil)
        
    }
    
    class func messageErrorWithHandler(viewController: UIViewController, completion: @escaping (_ result: Bool)->()){
        
        let alertController = UIAlertController(title: "Error!",
                                                message: "An unknown error has occurred, please try again later",
                                                preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Accept", style: .default) { _ in
            
            completion(true)
        }

        alertController.addAction(okAction)
        
        viewController.present(alertController, animated: true, completion: nil)
        
    }
    
    class func showMessageWithHandler(title: String, message:String, viewController: UIViewController, completion: @escaping (_ result: Bool)->()){
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        alertController.view.tintColor = #colorLiteral(red: 0.368627451, green: 0.3607843137, blue: 0.9019607843, alpha: 1)
        
        
        
        
        let okAction = UIAlertAction(title: "Accept", style: .default) { _ in
            
            completion(true)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
}
