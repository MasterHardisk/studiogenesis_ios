//
//  Constants.swift
//  MasterHardiskKit
//
//  Created by MasterHardisk on 18/2/21.
//

import UIKit

public class Constants
{
    public static let url_prefix_api = "/api/"
    public static let url_base = "http://192.168.68.110:8888"
    public static let url_api = url_base + url_prefix_api
    
    public static let url_login = "login"
    public static let url_users = "users"
    public static let url_update_password = url_users + "/update/password"
    public static let url_tickets = "tickets"
    public static let url_search_tickets = url_tickets + "/search/"
}
