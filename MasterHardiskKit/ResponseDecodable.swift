//
//  ResponseDecodable.swift
//  MasterHardiskKit
//
//  Created by MasterHardisk on 23/2/21.
//

import UIKit

enum ResponseDecodable {
    case success
    case failure
}
