//
//  Ticket.swift
//  MasterHardiskKit
//
//  Created by MasterHardisk on 21/2/21.
//

import UIKit

public class Ticket: NSObject, NSCoding
{
    public var id : Int!
    public var name : String!
    public var details: String!
    public var event_date: Date!
    public var price: Int!
    public var images: [TicketImage] = []
    
    public init(json: JSON) {
        super.init()
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.details = json["description"].stringValue
        self.event_date = Utils.dateFormatter(dateFormat: "dd-MM-yyyy HH:mm").date(from: json["event_date"].stringValue)
        self.price = json["price"].intValue
        
        let imagesJson = JSON(json)["images_ticket"].array!
        
        for i in 0..<imagesJson.count{
            self.images.append(TicketImage(json: imagesJson[i]))
        }
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(name, forKey: "name")
        coder.encode(details, forKey: "details")
        coder.encode(event_date, forKey: "event_date")
        coder.encode(price, forKey: "price")
    }
    
    public required init?(coder aDecoder: NSCoder) {
        aDecoder.decodeObject(forKey: "id")
        aDecoder.decodeObject(forKey: "name")
        aDecoder.decodeObject(forKey: "details")
        aDecoder.decodeObject(forKey: "event_date")
        aDecoder.decodeObject(forKey: "price")
    }
    
}
