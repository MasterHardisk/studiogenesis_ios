//
//  TicketImage.swift
//  MasterHardiskKit
//
//  Created by Gerard Contador on 22/2/21.
//

import UIKit

public class TicketImage: NSObject, NSCoding
{
    public var id : Int!
    public var path : String!

    
    public init(json: JSON) {
        super.init()
        self.id = json["id"].intValue
        self.path = json["path"].stringValue
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(path, forKey: "path")
    }
    
    public required init?(coder aDecoder: NSCoder) {
        aDecoder.decodeObject(forKey: "id")
        aDecoder.decodeObject(forKey: "path")
    }
    
}
