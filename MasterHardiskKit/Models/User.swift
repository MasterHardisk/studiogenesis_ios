//
//  User.swift
//  MasterHardiskKit
//
//  Created by MasterHardisk on 18/2/21.
//

import UIKit

public class User: NSObject, NSCoding
{
    public var id : Int!
    public var name : String!
    public var surname : String!
    public var email : String!
    public var b_day: Date?
    public var profile: String?
    
    public init(json: JSON) {
        super.init()
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.surname = json["surname"].stringValue
        self.email = json["email"].stringValue
        self.b_day = Utils.dateFormatter(dateFormat: "dd-MM-yyyy").date(from: json["b_day"].stringValue)
        self.profile = json["image_profile"].stringValue
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(name, forKey: "name")
        coder.encode(surname, forKey: "surname")
        coder.encode(email, forKey: "email")
        coder.encode(b_day, forKey: "b_day")
        coder.encode(profile, forKey: "profile")
    }
    
    public required init?(coder aDecoder: NSCoder) {
        aDecoder.decodeObject(forKey: "id")
        aDecoder.decodeObject(forKey: "name")
        aDecoder.decodeObject(forKey: "surname")
        aDecoder.decodeObject(forKey: "email")
        aDecoder.decodeObject(forKey: "b_day")
        aDecoder.decodeObject(forKey: "profile")
    }

}
