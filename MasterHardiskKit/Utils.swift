//
//  Utils.swift
//  MasterHardiskKit
//
//  Created by MasterHardisk on 21/2/21.
//

import UIKit

public class Utils{
    
    public class func dateFormatter(dateFormat: String)->DateFormatter{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: Locale.current.identifier)
        dateFormatter.locale = Locale.current
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = dateFormat
           
        return dateFormatter
    }
}
